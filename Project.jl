function printsum(a)
  println(summary(a),":",repr(a))
end

abstract ArbitraryPrecision

type apn <: ArbitraryPrecision
  sign::Bool
  expn::Int64
  n_max::Int64
  a_array::Array{Int64,1}
  len::Int64
  #constructor
  function apn(sign::Bool, expn::Int64, n_max::Int64, a_array::Array{Int64,1}, len::Int64)
    array = zeros(n_max+1)

    for i = 1:length(a_array)
      array[i] = a_array[i]
    end
        
    new(sign, expn, n_max, array, len)
  end
end

# apntoreal(apn) express an arbitrary precision number as a computer precision number
function apntoreal(a::ArbitraryPrecision)
  eval = 0
  len = a.n_max+1
  for i in 1:len
        eval = eval + a.a_array[i]*1000^(i-1)
  end
  eval = eval*1000^a.expn
  if !a.sign
    eval = -eval
  end
  eval
end

function change(a::ArbitraryPrecision,b::ArbitraryPrecision)
    ch = a
    a = b
    b = ch
end

function +(a::ArbitraryPrecision,b::ArbitraryPrecision)

  c = apn(true,0,a.n_max,Int64[0],0)

  if a.expn < b.expn #secure that a has higher exponent
    change(a,b)
  end

  i = a.len
  while a.expn != b.expn
    a[i+1] = a[i]
    a[1] = 0
    a.expn--
    a.len++
    i = i-1
  end

  c.expn = b.expn

  if a.len < b.len
    change(a,b)
  end

  delta = 0

  for i = 1:(a.len+1)
    c.a_array[i] = a.a_array[i] + b.a_array[i]

    if c.a_array[i] > 1000
      c.a_array[i] = c.a_array[i]-1000
      delta = 1
    else
      delta = 0
    end
  end

  if delta == 0
    c.len = a.len
  else
    c.len = a.len+1
    c.a_array[end] = 1
  end

  if a.sign == 1 #both numbers are positive
    c.sign = false

  else # both are negative
    c.sign = true
  end

  return c

end

g = apn(true,0,4,[400,321,189],2)

g + g