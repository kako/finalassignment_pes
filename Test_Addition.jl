# apntoreal(apn) express an arbitrary precision number as a computer precision number
function apntoreal(a::apn)
  eval = 0
  len = n_max+1
  for i in 1:len
    eval = eval + a.a_array[i]*1000^(len-i)
  end
  eval = eval*1000^a.expn
  if !a.sign
    eval = -eval
  end
  eval
end

# test addition

# using ordinary computer precision numbers
tol = 1e-15

# case 1: 0 + 0 = 0
answer = 0

a = apn(0,0,0,[0],0)
b = apn(0,0,0,[0],0)
sum = a + b
res = apntoreal(sum)

relerr = abs(sum-answer)/answer
condition = sprintf('%0.16g <= %0.16g', relerr, tol)
testName = sprintf('%d + %d = %d',0,0,0);
myAssert(condition, testName, nameFunction, lineNumber);

# case 2
answer = 0

a = apn(0,0,0,[0],0)
b = apn(0,0,0,[0],0)
sum = a + b
res = apntoreal(sum)

relerr = abs(sum-answer)/answer
condition = sprintf('%0.16g <= %0.16g', relerr, tol)
testName = sprintf('%d + %d = %d',0,0,0);
myAssert(condition, testName, nameFunction, lineNumber);

# case 3

# case 4

# case 5