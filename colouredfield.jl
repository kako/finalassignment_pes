# coloured_field function

#include the type
include("ArbitraryPrecision.jl")

#lib to generate the images
using Winston


function colorTheFrame(R::Int64,iMax::Int64,nMax::Int64,nmbrOfFrame::Int64)
# function that performs escape time algo from one frame

  # STEP 1: initialization of parameters

  i_max = iMax # if i > i_max, then the sequence "diverges"
  n_rd = nMax # initial precision

  # Option 0: Centre of the zoom
  # just to test the first frame
  #x0_data = 0
  #y0_data = 0

  # Option 1: Centre of the zoom
  x0_data = [922,332,781,37,94,702,765,605,719,375,271,975,763,5] # SIGN!
  y0_data = [310,259,835,87,457,643,270,873,749,591,772,483,601,0]

  if length(x0_data) > n_rd # truncate the digits of strat point to current precision
    x0 = apn(false,-(n_rd),n_rd,x0_data[n_rd:-1:1],n_rd)
    y0 = apn(false,-(n_rd),n_rd,y0_data[n_rd:-1:1],n_rd) # change sign to flip the image
  else
    x0 = apn(false,-(length(x0_data)),n_rd,x0_data[end:-1:1],length(x0_data))
    y0 = apn(false,-(length(y0_data)),n_rd,y0_data[end:-1:1],length(y0_data)) # change sign to flip the image
  end

  # Option 2: Centre of the zoom
  # x0_data = [636,754,346,582,389,978,643,739,831,915,714,49,947,203,602,138,502,943,775,952,881,544,20,962,497,567,512,237,
  #           982,520,103,84,82,959,106,578,523,873,70,993,324,732,992,925,284,558,883,390,86,339,317,126,456,342,587,826,
  #           887,851,188,953,461,652,529,120,2] # SIGN!
  # y0_data = [685,31,297,83,677,301,476,87,839,141,743,767,421,310,109,887,775,75,70,904,950,845,270,345,674,851,348,147,
  #           467,91,923,327,25,557,225,49,846,615,654,152,705,267,539,941,2,138,761,943,853,722,265,761,560,288,190,187,
  #           874,239,788,845,55,193,77,244,66]

  h = apn(true,-1,n_rd,[int64(4000/R)],1) # generate starting ((-2,-1),(2,1)) rectangle
  k_factor = apn(true,-1,n_rd,[900],1)   # zooming factor

  #multiply h acording to zooming factor
  multiply_Xtimes = nmbrOfFrame
  while multiply_Xtimes != 0
    h = h*k_factor
    multiply_Xtimes = multiply_Xtimes -1
  end

  # STEP 2: initialization of the field
  field = zeros(R-1,int64(R)+1) # Array{Float64,2}

  # STEP 3: loop in the field
  aux = apn(false,-1,n_rd,[3],1)
  limit = apn(true,0,n_rd,[4],1)

  for i = 1:size(field,1)

    for j = 1:size(field,2)

      if abs(i-int64(R/2)) == 0
        Px = apn(true,0,n_rd,[0],0)
      else
        Px = apn(i-int64(R/2) >= 0,0,n_rd,[abs(i-int64(R/2))],1)
      end

      if abs(j-(int64(R/2)+1)) == 0
        Py = apn(true,0,n_rd,[0],0)
      else
        Py = apn(j-(int64(R/2)+1) >= 0,0,n_rd,[abs(j-(int64(R/2)+1))],1)
      end

      #xc = Px*h + aux to correct the pixel error at x = 0 -strange issue
      xc = Px*h + x0
      yc = Py*h + y0

      x = apn(true,0,n_rd,[0],0)
      y = apn(true,0,n_rd,[0],0)

      k = 1

      while x*x + y*y <= limit && k < i_max

        temp = x*x - y*y + xc
        y = x*y + x*y + yc
        x = temp

        x = fact(x)
        y = fact(y)

        k = k + 1
      end

      #if k == 4 && i == 100 && j == 1
        #print(j, " ", xc, " ", yc, "\n")
        #print(j, " ", x, " ", y, " ", x*x + y*y, "\n")
        #print(j, " ", apntoreal(x), " ", apntoreal(y), " ", apntoreal(x*x + y*y), "\n")
      #end

      field[i,j] = k
    end
  end

  return field
end


function generateZoom(startFrame::Int64,endFrame::Int64,nmbrOfColours::Int64)
#function that performs zooming frame by frame

  resolution = 100
  iMax = 400
  colormap("jet",nmbrOfColours)
  refineFactor = 64 # according to Eric's computation :)

    for i = startFrame:endFrame
      # we still experience some errors so this is temporary work around
      try
        frame = colorTheFrame(resolution,iMax,3+div(i,refineFactor),i)
        iMax = int64(minimum(frame)*100)
        frame = log10(frame)
        imagesc(transpose(frame))
        name = string("frame",string(i),".png")
        savefig(name,width=400,height=400)
      catch error
        print("\nframe number ",i," was not generated !")
        print("\n The error was caused by: ",error,"\n")
      end
    end
end


cd("\MandelbrotSet")

generateZoom(29,65,100)


