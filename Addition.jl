# arithmetic operations

# addition
function +(a::ArbitraryPrecision,b::ArbitraryPrecision)

  c = apn(true,0,a.n_max,Int64[0],0)

  if a.expn < b.expn #secure that a has higher exponent
    ch = a
    a = b
    b = ch
  end

  while a.expn != b.expn
    i = a.len + 1
    while i != 0
        a.a_array[i+1] = a.a_array[i]
        i = i -1
    end
    a.a_array[1] = 0
    a.expn = a.expn - 1
    a.len = a.len + 1
  end

  c.expn = b.expn

  if a.len < b.len
    ch = a
    a = b
    b = ch
  end
    
  delta = 0

  for i = 1:(a.len+1)
    
    c.a_array[i] = a.a_array[i] + b.a_array[i] + delta

    if c.a_array[i] > 1000
            
      c.a_array[i] = c.a_array[i]-1000
      delta = 1
            
    else
            
      delta = 0
            
    end
  end

  if delta == 0
        
    c.len = a.len
        
  else
        
    c.len = a.len+1
    c.a_array[end] = 1
        
  end

  if a.sign == 1 #both numbers are positive
        
        c.sign = true
        
  else # both are negative
        
        c.sign = false
        
  end

  return c

end
