# Arithmetic Precision Numbers:
# Data Structure, Arithmetic Operations, Auxiliary Methods.

# Definition of the data structure
abstract ArbitraryPrecision

type apn <: ArbitraryPrecision
  sign::Bool
  expn::Int64
  n_max::Int64
  a_array::Array{Int64,1}
  len::Int64
  # constructor
  function apn(sign::Bool, expn::Int64, n_max::Int64, a_array::Array{Int64,1}, len::Int64)
    array = zeros(n_max+1)

    for i = 1:length(a_array)
      array[i] = a_array[i]
    end

    new(sign, expn, n_max, array, len)
  end
end


# auxiliary methods
function apntoreal(a::ArbitraryPrecision)
# apntoreal(apn) express an arbitrary precision number as a computer precision number

  eval = 0.
  len = a.n_max+1
  for i in 1:len
    eval = eval + a.a_array[i]*1000^float64(i-1)
  end

  eval = eval*1000^float64(a.expn)

  if !a.sign
    eval = -eval
  end
  eval
end

# swaps two apns
function change(a::ArbitraryPrecision,b::ArbitraryPrecision)
  # the function has to swap each parameter of apn one by one since apn are bound to imutable

  #exchange the signs
  sign = a.sign
  a.sign = b.sign
  b.sign = sign

  #exhange the exponent
  expn = a.expn
  a.expn = b.expn
  b.expn = expn

  #echange the nMax
  nMax = a.n_max
  a.n_max = b.n_max
  b.n_max = nMax

  #echange the array
  array = a.a_array
  a.a_array = b.a_array
  b.a_array = array

  #encahge the len
  len = a.len
  a.len = b.len
  b.len = len
end

  # bring a and b to same exponent
# function same_expn(a::ArbitraryPrecision, b::ArbitraryPrecision)
#   i = a.len
#   while a.expn != b.expn
#     a[i+1] = a[i]
#     a[1] = 0
#     a.expn--
#     a.len++
#     i = i-1
#   end
# end

# comparison of two abs numbers
function <=(a::ArbitraryPrecision, b::ArbitraryPrecision)

  a_is_eq_or_smaller = false

  if (a.len + a.expn < b.len + b.expn)
    a_is_eq_or_smaller = true
  elseif (a.len + a.expn > b.len + b.expn)
    a_is_eq_or_smaller = false
  else
    #need to bring to same exponent
    if a.expn < b.expn #secure that a has higher exponent
      change(a,b)
      abChanged = true
    else
      abChanged = false
    end

    i = 0
    while (a.expn-b.expn > a.n_max+1-a.len)

      if b.a_array[1] < 500
        b.a_array[1:end-(i+1)] = b.a_array[2:end-i]
      else
        b.a_array[1:end-(i+1)] = b.a_array[2:end-i]
        b.a_array[1] = b.a_array[1] + 1
      end
      b.a_array[end-i] = 0

      b.expn = b.expn + 1
      b.len = b.len - 1
      i = i + 1
    end

    while a.expn != b.expn
      i = a.len
      while i != 0
          a.a_array[i+1] = a.a_array[i]
          i = i -1
      end
      a.a_array[1] = 0
      a.expn = a.expn - 1
      a.len = a.len + 1
    end

    #change a and b back so the <= has same meaning
    if abChanged
      change(a,b)
    end

    for i = a.len:-1:1
      if (a.a_array[i] < b.a_array[i])
        a_is_eq_or_smaller = true
        break
      elseif (a.a_array[i] > b.a_array[i])
        a_is_eq_or_smaller = false
        break
      elseif i == 1
          a_is_eq_or_smaller = true
      else
          a_is_eq_or_smaller = false
      end
    end
  end

  #decied based on signs
  return a_is_eq_or_smaller
end

# arithmetic operations

# addition
function +(x::ArbitraryPrecision,y::ArbitraryPrecision)

   a = deepcopy(x) # input should not be changed
   b = deepcopy(y)

  c = apn(true,0,a.n_max,Int64[0],0)

  #avoid degenerated operations
  if sum(abs(a.a_array) + abs(b.a_array)) == 0
    #if b.len > 0
      #print(b, "\n")
    #end
    return apn(true,0,a.n_max,Int64[0],0)
  end

  if sum(abs(a.a_array)) == 0
    #if a.len > 0
    #print(a, "\n")
    #end
    return b
  end

  if sum(abs(b.a_array)) == 0
    #if b.len > 0
    #print(b, "\n")
    #end
    return a
  end

  #check the signs to decide if it is addition or substrasction
  if a.sign == 1 && b.sign == 0
    b.sign = 1
    return a-b
  end

  if a.sign == 0 && b.sign == 1
    a.sign = 1
    return b-a
  end

  if a.expn < b.expn #secure that a has higher exponent
    change(a,b)
  end

  i = 0
  while (a.expn-b.expn > a.n_max+1-a.len)

    if b.a_array[1] < 500
      b.a_array[1:end-(i+1)] = b.a_array[2:end-i]
    else
      b.a_array[1:end-(i+1)] = b.a_array[2:end-i]
      b.a_array[1] = b.a_array[1] + 1
    end
    b.a_array[end-i] = 0

    b.expn = b.expn + 1
    b.len = b.len - 1
    i = i + 1

  end

  while a.expn != b.expn
    i = a.len
      while i != 0
        a.a_array[i+1] = a.a_array[i]
        i = i -1
      end
    a.a_array[1] = 0
    a.expn = a.expn - 1
    a.len = a.len + 1
  end

  c.expn = b.expn

  if a.len < b.len
    change(a,b)
  end

  delta = 0

  for i = 1:a.len

    c.a_array[i] = a.a_array[i] + b.a_array[i] + delta

    if c.a_array[i] > 1000

      c.a_array[i] = c.a_array[i]-1000
      delta = 1

    else

      delta = 0

    end
  end

  if delta == 0

    c.len = a.len

  else

    if a.len == a.n_max+1

      c.a_array[1:end-1] = c.a_array[2:end]
      c.a_array[end] = 1
      c.expn = c.expn + 1
      c.len = a.n_max+1

    else

      c.len = a.len+1
      c.a_array[c.len] = 1
    end
  end

  if a.sign == 1 && b.sign ==1 #both numbers are positive
        c.sign = true

  else # both are negative
        c.sign = false
  end

  return c
end


# subtraction
function -(x::ArbitraryPrecision, y::ArbitraryPrecision)

   a = deepcopy(x) # input should not be changed
   b = deepcopy(y)

  #avoid degenerated operations
  if sum(abs(a.a_array) + abs(b.a_array)) == 0
    #if b.len > 0
      #print(b, "\n")
    #end
    return apn(true,0,a.n_max,Int64[0],0)
  end

  if sum(abs(a.a_array)) == 0
    #if a.len > 0
    #print(a, "\n")
    #end
    b.sign = true
    return b
  end

  if sum(abs(b.a_array)) == 0
    #if b.len > 0
    #print(b, "\n")
    #end
    return a
  end

  #check the sgins to decide if it is addition or substrasction
  if a.sign == 1 && b.sign == 0
    b.sign = 1
    return a+b
  end

  if a.sign == 0 && b.sign == 1
    b.sign = 0
    return a+b
  end

  if a.sign == 0 && b.sign == 0
    b.sign = 1
    a.sign = 1
    return b-a
  end

  #check if a is larger than b
  if b<=a
    c = apn(true,0,a.n_max,Int64[0],0)
  else
    change(a,b)
    c = apn(false,0,a.n_max,Int64[0],0)
  end

  if a.expn < b.expn #secure that a has higher exponent
    change(a,b)
  end

  i = 0
  while (a.expn-b.expn > a.n_max+1-a.len)

    if b.a_array[1] < 500
      b.a_array[1:end-(i+1)] = b.a_array[2:end-i]
    else
      b.a_array[1:end-(i+1)] = b.a_array[2:end-i]
      b.a_array[1] = b.a_array[1] + 1
    end
    b.a_array[end-i] = 0

    b.expn = b.expn + 1
    b.len = b.len - 1
    i = i + 1

  end

  while a.expn != b.expn
    i = a.len
    while i != 0
        a.a_array[i+1] = a.a_array[i]
        i = i -1
    end
    a.a_array[1] = 0
    a.expn = a.expn - 1
    a.len = a.len + 1
  end

  c.expn = b.expn

  # make sure a has bigger length
  if a.len < b.len
    change(a,b)
  end

  delta = 0

  for i = 1:a.len
    c.a_array[i] = a.a_array[i] - b.a_array[i] + delta

    if c.a_array[i] < 0
      c.a_array[i] = c.a_array[i]+1000
      delta = -1
    else
      delta = 0
    end
  end

  c.len = c.n_max+1
  #check for zeros in front
  i=0
  if sum(abs(c.a_array)) > 0
    while c.a_array[end - i] == 0
      c.len=c.len-1
      i = i + 1
    end
  else
    c.len = 0
  end

  return c
end

# multiplication

function *(x::ArbitraryPrecision,y::ArbitraryPrecision)

  a = deepcopy(x) # input should not be changed
  b = deepcopy(y)

  #avoid degenerated operations
  if sum(abs(a.a_array)) == 0 || sum(abs(b.a_array)) == 0
    return apn(true,0,a.n_max,Int64[0],0)
  end

  c = apn(true,0,a.n_max,Int64[0],0)

  if a.len < b.len
    change(a,b)
  end

  for i = 1:b.len

    c_plus = apn(true,i-1,a.n_max,Int64[0],0)
    delta = 0

    for j = 1:a.len

      c_plus.a_array[j] = a.a_array[j]*b.a_array[i] + delta

      if c_plus.a_array[j] >= 1000

        delta = trunc(c_plus.a_array[j]/1000)
        c_plus.a_array[j] = c_plus.a_array[j] - delta*1000

      else
        delta = 0
      end
    end

    if delta == 0
      c_plus.len = a.len
    else
      if a.len == a.n_max+1

        c_plus.a_array[1:end-1] = c_plus.a_array[2:end]
        c_plus.a_array[end] = delta
        c_plus.expn = c_plus.expn + 1
        c_plus.len = a.n_max+1
      else

        c_plus.a_array[a.len + 1] = delta
        c_plus.len = a.len + 1
      end
    end

    c = +(c,c_plus)
  end

  c.sign = a.sign == b.sign # = a.sign*b.sign
  c.expn = a.expn + b.expn + c.expn

  return c
end


# factorize
function fact(x::ArbitraryPrecision)

  a = deepcopy(x) # input should not be changed

  if sum(abs(a.a_array)) > 0
    i = 0
    while a.a_array[1] == 0

      a.a_array[1:end-(i+1)] = a.a_array[2:end-i]
      a.a_array[end-i] = 0

      a.expn = a.expn + 1
      a.len = a.len - 1
      i = i + 1
    end
  end

  return a
end
