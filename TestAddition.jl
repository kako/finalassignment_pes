# include data structure and operations
#push!(LOAD_PATH,"finalassignment_pes/")

#include the type
include("ArbitraryPrecision.jl")

#testing package
using Base.Test

# test methods

# addition
function TestAddition(a::ArbitraryPrecision,b::ArbitraryPrecision,answer::Real)
  tol = 1e-08 # redefined each time you call =(
  eps = 1e-12 # same comment applies
  sum = apntoreal(a + b)
  return abs(sum-answer) < tol*abs(answer) + eps
end


function TestSubstraction(a::ArbitraryPrecision,b::ArbitraryPrecision,answer::Real)
  tol = 1e-08 # redefined each time you call =(
  eps = 1e-12 # same comment applies
  sum = apntoreal(a - b)
  return abs(sum-answer) < tol*abs(answer) + eps
end


function TestMultiplication(a::ArbitraryPrecision,b::ArbitraryPrecision,answer::Real)
  tol = 1e-08 # redefined each time you call =(
  eps = 1e-12 # same comment applies
  sum = apntoreal(a * b)
  return abs(sum-answer) < tol*abs(answer) + eps
end


function GenerateApn(lenghtOfArray::Int,lowerExpn::Int,UpperExpn::Int)

  #generation of parameters for apn
  randSign = rand(Bool)
  randExpn = -lowerExpn +  mod(rand(Int64,1),UpperExpn)[1]
  randLength = 1 + mod(rand(Int64,1),lenghtOfArray)[1]
  randArray = mod(rand(Int64,randLength),1000)
  #check if the last generated number in randArray is zero
  if randArray[end] == 0
    randArray[end] = 1 +  mod(rand(Int64,1),999)[1]
  end

  return apn(randSign,randExpn,30,randArray,randLength)
end


function TestNumericalOperations(numberOfTests::Int,nMax::Int)

  for i = 1:numberOfTests

    # random apns with the exponent ranging from -2 to 5 and array length from 1 to 5
    a_test = GenerateApn(4,-2,5)
    b_test = GenerateApn(4,-2,5)

    a_real = apntoreal(a_test)
    b_real = apntoreal(b_test)

    # debug prints to see the test results
    print("\n=============== ",i,". simulation =============== \n a = ",a_test, " real_a = ",a_real)
    print("\n b = ",b_test," real_b = ",b_real)

    print("\n a+b = ",a_test+b_test, "  real_a+real_b = ",a_real+b_real)
    @test TestAddition(a_test,b_test,a_real+b_real)
    print("\n a-b = ",a_test-b_test, "  real_a-real_b = ",a_real-b_real)
    @test TestSubstraction(a_test,b_test,a_real-b_real)
    print("\n a*b = ",a_test*b_test, "  real_a*real_b = ",a_real*b_real)
    @test TestMultiplication(a_test,b_test,a_real*b_real)
  end
end


TestNumericalOperations(100,30)
